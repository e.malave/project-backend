const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const redis = require('redis')
const bluebird = require('bluebird')
const validator = require('validate.js')
const axios = require('axios');
const crypto = require('crypto')
const moment = require('moment')
const cors = require('cors')

// BLUEBIRD
bluebird.promisifyAll(redis.RedisClient.prototype)
bluebird.promisifyAll(redis.Multi.prototype)

//CORS
app.use(cors())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// REDIS
let redisClient = redis.createClient()
redisClient.on('connect', function() {
    console.log('Connected to redis')
})

// const redisClient = redis.createClient(6379, 'redis')
// redisClient.on('connect', () => console.log('Connected to Redis'))

// BODY PARSER
app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({ extended: true }))


app.post('/register', async function(req, res) {

    var constraints = {
        username: {
            presence: true
        },
        password: {
            presence: true
        }
    }

    var validation = validator(req.body, constraints)

    if (!empty(validation)) {
        var response = {
            error: true,
            message: 'BAD_REQUEST',
            data: validation
        }
        return res.status(400).json(response);
    }

    // OBTENGO ARREGLO CON LOS USUARIOS y funcion que devuelve el id incrementable
    const users = await getAllUsernames()
    var id = await autoIncrementNumber()

    req.body.id = id

    if (users.indexOf(req.body.username) == -1) {

        await createUser(req.body)

        var response = {
            error: false,
            message: 'OK',
            data: null
        }
        return res.status(200).json(response);

    } else {

        var response = {
            error: true,
            message: 'ERR_REGISTERED_USERNAME',
            data: req.body.username
        }
        return res.status(200).json(response);
    }

})

app.post('/login', async function(req, res) {

    var constraints = {
        username: {
            presence: true
        },
        password: {
            presence: true
        }
    }

    var validation = validator(req.body, constraints)

    if (!empty(validation)) {
        var response = {
            error: true,
            message: 'BAD_REQUEST',
            data: validation
        }
        return res.status(400).json(response);
    }

    var users = await getAllUsers()

    var userExist = false
    for (i in users) {
        if (users[i].username === req.body.username && users[i].password === req.body.password) {
            userExist = true
            break
        }
    }

    if (userExist) {

        var token = await generateToken()

        var response = {
            error: false,
            message: 'OK',
            data: {
                token: token
            }
        }
        return res.status(200).json(response);
    } else {
        var response = {
            error: true,
            message: 'INVALID_CREDENTIALS',
            data: null
        }
        return res.status(200).json(response);
    }
})

app.get('/rickandmortyapi', verifyToken, async function(req, res) {

    // names, status, species, gender and image.
    var data = await axios.get('https://rickandmortyapi.com/api/character/');

    var array = []
    for(i in data.data.results){
        var obj = data.data.results[i]
      
        var json = {
           id: obj.id,
           name : obj.name,
           status : obj.status,
           species: obj.species,
           gender: obj.gender,
           image: obj.image

        }
        array.push(json)
    }

    var response = {
        error: false,
        message: 'OK',
        data: array
    }
    return res.status(200).json(response);

})



//[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ FUNCIONES  ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]

async function getAllUsers() {

    var data = await redisClient.lrangeAsync(`users`, -100, 100)
    var users = []
        //parseo el json string 
    for (i in data) {
        users.push(JSON.parse(data[i]))
    }

    return users

}

async function getAllUsernames() {

    var users = await getAllUsers()

    // Obtener 
    var usernames = []
    for (i in users) {
        usernames.push(users[i].username)
    }
    return usernames
}

async function createUser(user) {

    var response = ""
    redisClient.lpush('users', JSON.stringify(user), function(err, reply) {
        if (err) {
            response = err
        }
        response = reply
    })

    return response
}


async function generateToken() {

    var token = crypto.randomBytes(16).toString('hex')

    var currentDate = moment().toDate()

    currentDate = moment(currentDate)
        .add(2, 'minutes')

    redisClient.setAsync(token, currentDate).then(redis.print);


    return token
}

async function autoIncrementNumber() {
    var data = await getAllUsers()
    id = data.length + 1
    return id
}

function empty(mixedVar) {

    var undef
    var key
    var i
    var len
    var emptyValues = [undef, null, false, 0, '', '0']

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true
        }
    }

    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false
            }
        }
        return true
    }

    return false
}

async function verifyToken(req, res, next) {

    var token = req.headers['authorization'];

    if (!token) {
        var response = {
            error: true,
            message: 'EMPTY_TOKEN'
        }
        return res.status(200).json(response);
    }

    token = token.replace('Bearer ', '');

    var expiresAt = await redisClient.getAsync(token).then(function(reply, err) {
        if (err) {
            console.log('ERROR' + err)
        }
        return reply
    })

    if (expiresAt === null) {
        var response = {
            error: true,
            message: 'INVALID_TOKEN'
        }
        return res.status(200).json(response);
    }

    var currentDate = moment().toDate()

    expiresAt = moment(expiresAt).format('X')
    currentDate = moment(currentDate).format('X')

    if (expiresAt <= currentDate) {
        var response = {
            error: true,
            message: 'EXPIRED_TOKEN'
        }
        return res.status(401).json(response);
    } else {
        next()
    }

}

var port = 8081

app.listen(port, function(req, res, next) {
    console.log('app listening on port ' + port)
})